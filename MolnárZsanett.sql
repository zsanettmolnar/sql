--create team table
if OBJECT_ID('Club') is not null drop table Club;
GO

CREATE TABLE Club (
	CLUB_ID int not null identity(1,1) primary key,
	NAME nvarchar(50),
	FOUNDED date,
	COUNTRY nvarchar(50),
	COACH nvarchar(50),
	WON_CHAMP_NO int
);
GO

--create player table
if OBJECT_ID('Player') is not null drop table Player;
GO

CREATE TABLE Player (
	PLAYER_ID int not null identity(1,1) primary key,
	NAME nvarchar(50),
	AGE int,
	HEIGHT int,
	WEIGHT float,
	NATIONALITY nvarchar(3),
	POSITION nvarchar(50),
	CLUB int foreign key references Club(CLUB_ID)
);
GO

--create stadion table
if OBJECT_ID('Stadion') is not null drop table Stadion;
GO

CREATE TABLE Stadion (
	STADION_ID int not null identity(1,1) primary key,
	NAME nvarchar(50),
	BUILT date,
	SEAT_NO int,
	COUNTRY nvarchar(50),
	CITY nvarchar(50),
	CONSTR_COST float,
	CLUB int foreign key references Club(CLUB_ID)
);
GO

--create contract table
if OBJECT_ID('Contracts') is not null drop table Contracts;
GO

CREATE TABLE Contracts (
	CONTRACT_ID int not null identity(1,1) primary key,
	from_date date,
	to_date date,
	player_id int foreign key references Player(PLAYER_ID),
	club_id int 
);
GO

--insert into tables
INSERT INTO Club VALUES ('Telekom Veszpr�m','1977-01-01','HUN','Ljubomir Vranjes',56);
INSERT INTO Club VALUES ('MOL Pick Szeged','1961-01-01','HUN','Juan Carlos Pastor',45);
INSERT INTO Club VALUES ('Rail-Cargo Ferencv�ros','1950-01-01','HUN','Elek G�bor',29);
INSERT INTO Club VALUES ('Gy�ri Audi ETO KC','1948-01-01','HUN','Ambros Martin',31);

INSERT INTO Player VALUES ('Mikler Roland',34,190,101,'HUN','goalkeeper',1);
INSERT INTO Player VALUES ('Nagy L�szl�',37,207,116,'HUN','right back',1);
INSERT INTO Player VALUES ('L�kai M�t�',30,190,89,'HUN','centre',1);
INSERT INTO Player VALUES ('Mirko Alilovic',33,200,110,'CRO','goalkeeper',1);
INSERT INTO Player VALUES ('Szucs�nszki Zita',31,172,63,'HUN','centre',3);
INSERT INTO Player VALUES ('Kovacsics Anik�',27,171,65,'HUN','centre',3);
INSERT INTO Player VALUES ('B�r� Blanka',28,185,65,'HUN','goalkeeper',3);
INSERT INTO Player VALUES ('B�nhidi Bence',23,204,110,'HUN','pivot',2);
INSERT INTO Player VALUES ('Bod� Rich�rd',28,203,100,'HUN','left back',2);

INSERT INTO Stadion VALUES ('Veszpr�m Ar�na','2008-07-01',6000,'Hungary','Veszpr�m',4700000000,1);
INSERT INTO Stadion VALUES ('Szegedi Sportcsarnok','2006-06-01',3097,'Hungary','Szeged',null,2);
INSERT INTO Stadion VALUES ('Audi Ar�na','2014-11-01',5500,'Hungary','Gy�r',5500000000,4);
INSERT INTO Stadion VALUES ('Elek Gyula Ar�na','1997-02-23',1300,'Hungary','Budapest',null,3);

-- + insert into contracts
INSERT INTO Contracts VALUES ('2005-01-01','2019-06-30',1,1);
INSERT INTO Contracts VALUES ('2006-07-01','2020-06-30',2,1);
INSERT INTO Contracts VALUES ('2007-01-01','2020-06-30',3,1);
INSERT INTO Contracts VALUES ('2012-07-01','2019-06-30',4,1);
INSERT INTO Contracts VALUES ('2015-07-01','2019-06-30',5,3);
INSERT INTO Contracts VALUES ('2002-07-01','2020-06-30',6,3);
INSERT INTO Contracts VALUES ('2014-07-01','2019-06-30',7,3);
INSERT INTO Contracts VALUES ('2005-01-01','2019-06-30',8,2);
INSERT INTO Contracts VALUES ('2008-07-01','2020-06-30',9,2);

--*************************************************************************************************************************
-- three simple table select

--write out all the players' name
SELECT * FROM Player;
--write out the name of the clubs
SELECT NAME FROM Club;
--write out the number of seats and the name of the country in which the stadion is found
SELECT SEAT_NO, COUNTRY FROM Stadion;

--*************************************************************************************************************************
-- 3 simple single-table GROUP BY

--write out the average weight of the players for each handball club
SELECT avg(WEIGHT) FROM Player group by CLUB;
--summerize the cost of the construction of the stadions listed in the stadion table
SELECT SUM(CONSTR_COST) FROM Stadion GROUP by UPPER(COUNTRY);
--write out the average height of the players for each position listed in the player table
SELECT avg(HEIGHT) FROM Player GROUP BY UPPER(POSITION);

--*************************************************************************************************************************
-- 5 multitable

--write out the player name, handball club name pairs
SELECT p.NAME, b.NAME FROM Player p, Club b WHERE p.CLUB = b.CLUB_ID;
--write out the couch of the handball club, and the matching city where the club plays
SELECT b.COACH, s.CITY FROM Club b, Stadion s WHERE s.CLUB = b.CLUB_ID;
--write out the player and stadion name pairs
SELECT p.NAME, s.NAME FROM Player p, Club c, Stadion s WHERE p.CLUB = c.CLUB_ID AND s.CLUB = c.CLUB_ID;
--There is a hadball club in the database, Gy�r Audi ETO KC, for which i did not inserted any player for this purpose.
--These commands list all the players with the name of their coach.
--With left join, the coach who has no players is not listed. With the right join, his name is in the list too.
SELECT p.NAME, c.COACH FROM Player p LEFT JOIN Club c ON p.CLUB = c.CLUB_ID; --Ambros Martin is not listed!
SELECT p.NAME, c.COACH FROM Player p RIGHT JOIN Club c ON p.CLUB = c.CLUB_ID; --Ambros Martin is listed!



--*************************************************************************************************************************
-- 5 analyics/advanced grouping query

--List all the club names in which there is a player who is taller than the average of the players playing in the listed clubs. 
SELECT c.NAME 
FROM Player p, Club c, (SELECT AVG(HEIGHT) as average FROM Player) as avgheight 
WHERE p.CLUB = c.CLUB_ID AND p.HEIGHT > avgheight.average 
GROUP BY c.NAME;

--List the expensive stadions. A stadion is expensive if the cost of the construction is over 4,5 billion HUF.
SELECT p.* 
FROM Player p, Club c, (SELECT CLUB as clubid FROM Stadion WHERE CONSTR_COST > 4500000000) as stadionmin
WHERE p.CLUB = c.CLUB_ID AND stadionmin.clubid = c.CLUB_ID;

--List the stadions (name and location) where it is possible to host big events like concerts. 
--A stadion is considered big if the number of seat is greater than 4000.
SELECT bigstadion.stadname, bigstadion.City_location 
FROM Club c, (SELECT NAME as stadname, CLUB as clubid, CITY as City_location FROM Stadion WHERE SEAT_NO > 4000) as bigstadion 
WHERE c.CLUB_ID = bigstadion.clubid;

--List the name of players and their club's name whose club won more than 40 champion since the club is founded
SELECT p.NAME, goodclub.CLUB_NAME 
FROM Player p, (SELECT CLUB_ID as clubid, NAME as CLUB_NAME FROM Club WHERE WON_CHAMP_NO > 40) as goodclub 
WHERE p.CLUB = goodclub.clubid;

--List the "hungarian" clubs! 
--A club in which the number of hungarian players is more than the 50% of the number of players.

--count the hungarian players by clubs
--SELECT COUNT(c.CLUB_ID) as howmany, c.CLUB_ID as clubid FROM Player p, Club c 
--WHERE p.CLUB = c.CLUB_ID and p.NATIONALITY = UPPER('HUN') GROUP BY c.CLUB_ID;
--count the players per clubs
--SELECT COUNT(p.PLAYER_ID) as pperc 
--FROM Player p, Club c WHERE p.CLUB = c.CLUB_ID GROUP BY c.CLUB_ID;

--the solution:
SELECT c.NAME 
FROM Club c,
(SELECT COUNT(c.CLUB_ID) as howmany, c.CLUB_ID as clubid 
FROM Player p, Club c WHERE p.CLUB = c.CLUB_ID and p.NATIONALITY = UPPER('HUN') GROUP BY c.CLUB_ID) as howmanyhun,
(SELECT COUNT(p.PLAYER_ID) as pperc, c.CLUB_ID as clubid 
FROM Player p, Club c WHERE p.CLUB = c.CLUB_ID GROUP BY c.CLUB_ID) as clubplayers
WHERE c.CLUB_ID = howmanyhun.clubid and c.CLUB_ID = clubplayers.clubid and  howmanyhun.howmany > (clubplayers.pperc/2);

--*************************************************************************************************************************
--5 complex subquery select

--List the players whose club's name begins with T
SELECT p.NAME
FROM Player p,
(SELECT CLUB_ID as clubid FROM Club WHERE NAME LIKE 'T%') as tclub
WHERE p.CLUB = tclub.clubid;

--List the coaches whose team plays in a stadion which name contains the letter e.
SELECT c.COACH
FROM Club  c,
(SELECT CLUB as ownclub FROM Stadion WHERE NAME LIKE '%E%') as estadion
WHERE c.CLUB_ID = estadion.ownclub;

-- List the players who play in the city, Szeged.
SELECT p.NAME
FROM Player p, Club c,
(SELECT CLUB as ownclub FROM Stadion WHERE CITY = UPPER('szeged')) as sziclub
WHERE p.CLUB = c.CLUB_ID AND c.CLUB_ID = sziclub.ownclub;

--List the players who plays in a team which name is longer than 4 characters. 
SELECT p.NAME
FROM Player p,
(SELECT c.CLUB_ID as clubid FROM Stadion s, Club c WHERE s.CLUB = c.CLUB_ID AND LEN(s.CITY) > 4) as longclub
WHERE p.CLUB = longclub.clubid;

--List the club names in which play non hungarian players.
SELECT c.NAME
FROM Club c,
(SELECT p.CLUB as pclub FROM Player p WHERE p.NATIONALITY <> UPPER('HUN')) as nothunp
WHERE c.CLUB_ID = nothunp.pclub;

--*************************************************************************************************************************
--Must use at least 10 different row-level functions and all grouping aggregate functions

--Count the number of players in each club
SELECT COUNT(c.CLUB_ID) as howmany, c.CLUB_ID as clubid 
FROM Player p, Club c WHERE p.CLUB = c.CLUB_ID GROUP BY c.CLUB_ID; 
--Write out the average contract duration for each club
SELECT AVG(DATEDIFF(YEAR,c.from_date,c.to_date)) as avgcontracts, cl.CLUB_ID 
FROM Player p, Contracts c, Club cl 
WHERE p.PLAYER_ID = c.CONTRACT_ID AND p.CLUB = cl.CLUB_ID AND c.club_id = cl.CLUB_ID 
GROUP BY cl.CLUB_ID;
--Write out the average seat number in the stadion stored in the database
SELECT AVG(SEAT_NO) FROM Stadion;
--Write out the shortest player height.
SELECT MIN(p.HEIGHT) FROM Player p;
--Write out the oldest club age.
SELECT MAX(DATEDIFF(YEAR,FOUNDED,GETDATE())) as oldest FROM Club;
--Write out the longest contract duration.
SELECT MAX(DATEDIFF(YEAR,c.from_date,c.to_date)) as contractlength 
FROM Player p, Contracts c WHERE p.PLAYER_ID = c.player_id;
--Count the number of player in the team, Rail Cargo Ferencv�ros
SELECT COUNT(p.PLAYER_ID) FROM Player p, Club c 
WHERE p.CLUB = c.CLUB_ID AND c.NAME = UPPER('Rail-Cargo Ferencv�ros');



BEGIN TRANSACTION
-- Set the weight of the player to 110 who is taller than 205 cm.
Select * from Player;
UPDATE Player SET WEIGHT = 110 WHERE PLAYER_ID IN ( Select PLAYER_ID FROM Player WHERE HEIGHT > 205);
Select * from Player;
ROLLBACK TRANSACTION;
Select * from Player;

BEGIN TRANSACTION;
--Set cost of the construnction to 4,5 billion HUF where the cost is unknown.
SELECT * FROM Stadion;
UPDATE Stadion SET CONSTR_COST = 4500000000 WHERE STADION_ID IN 
( Select STADION_ID FROM Stadion WHERE CONSTR_COST is null);
SELECT * FROM Stadion;
ROLLBACK TRANSACTION;
Select * from Stadion;

BEGIN TRANSACTION;
--Delete the oldest player
SELECT * FROM Player;
SELECT * FROM Contracts;
DELETE FROM Contracts WHERE CONTRACT_ID IN (Select TOP(1) CONTRACT_ID FROM Contracts co, Player p WHERE co.player_id = p.PLAYER_ID ORDER BY p.AGE desc);
DELETE FROM Player WHERE PLAYER_ID IN (Select TOP(1) PLAYER_ID FROM Player ORDER BY AGE desc);
SELECT * FROM Contracts;
SELECT * FROM Player;
ROLLBACK TRANSACTION;
Select * from Contracts;
Select * from Player;


BEGIN TRANSACTION;
--Bulldoze the oldest stadion.
SELECT * FROM Stadion;
DELETE FROM Stadion WHERE STADION_ID IN (Select TOP(1) STADION_ID FROM Stadion ORDER BY BUILT asc);
SELECT * FROM Stadion;
ROLLBACK TRANSACTION;
Select * from Stadion;

